Application Binding
===================

Registration
------------

1. On the navigation sidebar, select ‘Registration’ menu option.

.. image:: images/demo_registration.PNG
   :align: center

2. The ‘Customer Registration’ page will appear, please fill out 
   the detail.

.. image:: images/demo_customer_registration_form.PNG
   :align: center

3. Once the detail are confirmed, click ‘Create’.

Add Pilgrims
------------

1. On the navigation sidebar, select ‘Customer List’ menu option.

.. image:: images/demo_customer_list.PNG
   :align: center

2. In ‘Customer List’ table, find the verified customer and 
   click ‘Add Pilgrim’ button icon.

.. image:: images/demo_add_button_with_overlay.PNG
   :align: center

3. The ‘Pilgrim Registration’ page will appear, please fill out 
   the pilgrim’s detail.

.. image:: images/demo_pilgrim_form.PNG
   :align: center

4. Binding: In the ‘Wearable Information’ section, enter Raqib 
   IMEI Number, SIM Card Number and SIM Mobile Number as 
   provided in your registered email sent by Favoriot.

.. image:: images/demo_wearable_information.PNG
   :align: center

5. Once the pilgrim’s detail and wearable information are 
   confirmed, click ‘Create’.

6. To view the Customer detail, click the ‘View’ eye button icon.

7. To update the Customer detail, click the ‘Edit Customer’ 
   button icon.

8. To delete the Customer detail, click the ‘Delete Customer’ 
   button icon.