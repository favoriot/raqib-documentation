Add Mutawwif
============

1. On the navigation sidebar, select ‘Add Mutawwif’ menu option.

2. The ‘Mutawwif Registration’ page will appear, please fill out 
   the detail.

.. image:: images/demo_mutawwif_registration_form.PNG
   :align: center

3. Once the details are confirmed, click ‘Create’.