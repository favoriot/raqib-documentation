Pilgrim List
============

1. On the navigation sidebar, Select ‘Pilgrim List’ menu.

.. image:: images/demo_pilgrim_list.PNG
   :align: center

2. To view the Pilgrim detail, click the ‘View’ eye button icon.

3. To update the Mutawwif detail, click the ‘Edit Pilgrim’ button 
   icon.

4. To delete the Pilgrim detail, click the ‘Delete Pilgrim’ 
   button icon.