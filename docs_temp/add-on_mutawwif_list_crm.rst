Add-on: Mutawwif List
=====================

1. In the sidebar menu, select ‘Mutawwif List’ menu.

.. image:: images/demo_mutawwif_list.PNG
   :align: center

2. The ‘Mutawwif Listing’ page will appear with list of Mutawwif.

3. Click the ‘Expand Mutawwif’ button icon at the side of 
   respective Mutawwif Name.

.. image:: images/demo_table_of_registered_mutawwif.PNG
   :align: center

4. The table with group of pilgrims under the respective Mutawwif 
   supervision will appear.

5. To view the Mutawwif detail, click the ‘View’ eye button icon.

6. To update the Mutawwif detail, click the ‘Edit Mutawwif’ 
   button icon.

7. To delete the Mutawwif detail, click the ‘Delete Mutawwif’ 
   button icon.