Welcome to Raqib Documentation
==============================

Raqib documentation is comprised of tutorial and guide for 
different parts of services that are offered by Raqib. The 
main documentation for the site is organized into a couple 
sections:

* :ref:`mobile-app-docs`
* :ref:`wearable-docs`
* :ref:`dashboard-docs`
* :ref:`crm-docs`

.. _mobile-app-docs:

.. toctree::
   :maxdepth: 2
   :caption: Mobile App Documentation

   log_in_to_your_raqib_account_mobile
   reset_password_mobile
   raqib_overview_mobile
   view_latest_location_mobile
   view_heart_rate_mobile
   view_steps_mobile
   view_blood_pressure_mobile
   view_ecg_mobile
   view_menu_mobile
   view_pilgrims_information_mobile
   call_mutawwif_mobile
   view_alert_mobile
   view_about_mobile
   logging_out_mobile

.. _wearable-docs:

.. toctree::
   :maxdepth: 2
   :caption: Wearable Documentation

   flexi-1_and_button_overview_wearable
   charging_and_power_instruction_wearable
   application_installation_wearable
   home_screen_and_main_menu_wearable
   raqib_major_function_wearable

.. _dashboard-docs:

.. toctree::
   :maxdepth: 2
   :caption: Dashboard Documentation

   dashboard_01
   dashboard_02
   dashboard_03
   dashboard_04
   dashboard_05

.. _crm-docs:

.. toctree::
   :maxdepth: 2
   :caption: CRM Documentation

   sign_in_to_raqib_crm
   application_binding_crm
   pilgrim_list_crm
   add_mutawwif_crm
   add-on_mutawwif_list_crm

.. _quick-links:

.. toctree::
   :maxdepth: 2
   :caption: Quick Links

   Go to Raqib Website <http://raqib.co/>
   Go to Raqib Dashboard <http://tjc.dne.com.my/raqib/Login>
   Go to Raqib CRM <http://crm.raqib.co/#/login>