Sign in to Raqib CRM
====================

Raqib wearable requires Raqib CRM to bind and activate the 
service. You need to go to the `Raqib Official website`_.

.. _Raqib Official website: http://raqib.co/

1. In the website main menu, find the ‘Sign In’ menu and navigate 
   the dropdown to CRM; or directly navigate to the following 
   URL: http://crm.raqib.co 

.. image:: images/demo_landing_page.PNG
   :align: center

2. Enter your ‘User ID’ and ‘Password’ as provided in your 
   registered email sent by Favoriot.

.. image:: images/demo_crm_site.PNG
   :align: center

.. note:: If you lost your password, click at ‘Forgot password’ 
   option and follow the steps.