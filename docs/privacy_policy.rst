Privacy Policy for Raqib
========================

This page describes on how personal information is handled by Favoriot Sdn. Bhd.

Favoriot Sdn. Bhd. will handle your personal information (“Personal Data” as defined 
below) in accordance with the terms of this privacy policy (this “Privacy Policy”) and 
keep them confidential. Please read this Privacy Policy and understand the ways in which 
your Personal Data will be collected, used, processed and disclosed. The Company reserves 
the right to revise this Privacy Policy as it deems appropriate and the updated versions 
will be posted on the Website (www.raqib.co).

Favoriot's Privacy Position can be summarized as follows: (1) We are open about how we 
collect and use your personal information. (2) We are committed to using your personal 
information to provide you with better and more relevant services. (3) We always take 
steps to ensure that we keep your personal information safe and secure.

General
-------

We value your privacy and take the protection of your personal information seriously, 
so it is important to us that you understand how we collect and process personal 
information about you. This privacy notice applies to the processing of the personal 
information that we collect about you when you use any of our services, including our 
telecommunication networks, applications or any of our websites (collectively referred 
to as “services”). Please read it in conjunction with the Terms of Service of any 
service that you use, which may set out additional service-specific terms regarding 
the personal information we collect about you.

This privacy notice explains what personal information we collect about you, why and 
how we collect and process it, and how we share it with others. It also explains the 
privacy rights that you have in relation to your personal information and how you can 
exercise these rights.

Key Privacy Principle
---------------------

We may collect your name, Identification Number, address, email address, birth date, 
gender, contact numbers, occupation, credit or debit card information, travel 
information, personal interests, business registration number (if any), etc. (“Personal 
Data”). As part of the service offered under Raqib, we may also collect your vital 
health parameters such as heart rate, ECG, BP and location, number of steps and fall 
detection. This information may be relayed using SMS and / or data service to our 
system. If your Personal Data is stolen, hacked, lost or damaged due to accident or by 
any other means, without any fault or negligence on the part of the Company, the 
Company may not be responsible for any damages incurred to the extent permitted under 
Malaysia Law.

Purpose of Use of Personal Data
-------------------------------

All Personal Data collected may be used by the Company individually or collectively and 
may be combined with other information for, but not limited to the following purposes:

a) Marketing and communicating with you in relation to products, events and services 
   offered by the Company;
b) Contacting you for product or customer satisfaction surveys and/or market research 
   or promotions;
c) Developing and maintaining smooth relations with business partners and related 
   entities as a reference in the recruitment and responding to your job/application or 
   your recruitment issues;
d) Responding to business operation issues;
e) To arrange the product for delivery with the courier company;
f) To transfer or disclose the Personal Data to the Third Party or Sub-Contractor or 
   Personal Data collection center for data management purposes; and
g) To manage users' accounts.

Disclosure of Personal Data
---------------------------

Unless required or permitted under Malaysia law or as stated below, we will not disclose 
your Personal Data to others without your consent. In case the Company requires 
additional Personal Data, the Company will ask for such additional Personal Data from 
you and ensure its confidentiality.

We may disclose your Personal Data under, amongst others but not limited to, the 
following circumstances:

a) In certain promotional campaigns, where the campaign sponsors may ask for additional 
   Personal Data;
b) In certain cases, where you may be offered information or special news from a third 
   party (once you agree to receive such information, your name and email address will 
   be disclosed to the third party and you will be deemed to have consented to such 
   disclosure); and/or
c) In certain cases, where the Company may disclose information to inform a courier 
   company or make travel arrangements.

Personal Data provided to sub-contractors may be disclosed and offered to sub-contractors 
within the limited scope necessary to achieve the purpose for which the information is 
being used. (E.g. when the Company sends products to a distributor or assigns the role 
of organizing a campaign to a data processing company, a confidentiality agreement shall 
be signed by the third party and the Company shall strictly monitor and supervise the 
handling of the Personal Data by the third party).

In some cases when the Company needs to disclose your Personal Data to a third party, 
you are deemed to have agreed to provide your Personal Data in order for the Company 
and the third party to further respond to your inquiries or your requests, and/or to 
provide necessary service to you.

You will always have the right to approve or disapprove of such disclosures, or even to 
refuse to receive those services at any time.

If you wish to withdraw your consent and to unsubscribe from any future correspondences, 
please email us, otherwise, we assume that you agree to our processing of your personal 
data in accordance with the above.

The Company may disclose information in the following special cases without your consent:

a) If the disclosure is necessary for any investigation or proceedings;
b) If the disclosure is necessary for evaluative purposes;
c) If the disclosure is necessary for the Company to obtain legal services; or
d) If the disclosure is required by any applicable law, rule or regulation.

Acceptance of the Policy
------------------------

You are given a notice that the products and/or services will only be made available to 
you upon your accepting and expressly consenting to the terms of this Privacy Policy, 
where such express acceptance and consent shall be evidenced by you clicking, checking 
or indicating accordingly on the relevant consent portion of the registration forms or 
such other documents as to be furnished to you, as the case may be.

By so indicating your acceptance of the term of this Privacy Policy, you shall be deemed 
to have expressly consented to the processing of your personal data by the Company or 
any of its authorized agents, employees, partners and/or contractors for purposes as 
outlined in this Privacy Policy.

You hereby agree and accept that by registering and/or continuing to use the products 
and/or services, you authorize and consent to your Personal Data being processed by 
and where required, disclosed to classes of third parties as identified by the Company 
for the purposes of the Company providing the products and/or services to you. For the 
avoidance of doubt, you also hereby explicitly consent to the Company processing any 
sensitive Personal Data relevant to such purposes.

Your Choices in Personal Data Collection, Usage, Process and Disclosure
-----------------------------------------------------------------------

If you consent to receive information about the Company's products and services by mail, 
SMS, phone, email and fax, we will contact you by any of those communication channels to 
inform you about our news, activities, goods and services.

If you have any questions or inquiries concerning your Personal Data, or if you wish to 
withdraw your consent for the collection, use or disclosure of your Personal Data, 
please complete our personal data request form, and post a copy of the duly completed 
form to us via e-mail or fax. Our data protection officer will be pleased to attend to 
your queries and/or concerns.

If you do not consent to us collecting, using, processing, or disclosing your Personal 
Data, which is reasonable for provision of certain goods or services, we may not be able 
to provide the said goods or services.

Caution about Loss and Inappropriate Use of Personal Data
---------------------------------------------------------

Please note that sending information over the internet is not 100% secured. The Company 
will use reasonable endeavors to secure all information flow over the internet but cannot 
guarantee the security of any information transmitted over the internet in any case, 
whether on data inflow to the Company or outflow from the Company.

Governing Law
-------------

This Privacy Policy shall be governed and construed in accordance with the Malaysia Law. 
Any dispute in relation to the Website, and all rights and obligations and all actions 
contemplated by this Privacy Policy shall be submitted to the exclusive jurisdiction of 
the courts in Malaysia.

Right to Amend Our On-Line Privacy Statement
--------------------------------------------

The Company reserves the right to amend this Statement at any time. Changes will be 
effective immediately upon posting on the Website.

By publishing this Privacy Policy in the Website, we shall deem our customers and/or 
clients have been duly notified.

.. admonition:: Last Modified

   17th May 2018