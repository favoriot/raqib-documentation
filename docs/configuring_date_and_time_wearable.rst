Configure Date and Time
=======================

.. Important:: Please perform this activity when you first 
   receive the device, and when you go to places with different 
   time zone (e.g., when you reached Mecca).

.. note:: This instruction is for Flexi (F-1) user only.

1. Swipe right or left until you reach Setting page

.. image:: images/configuring_date_and_time_wearable_1.png
   :align: center

2. Tap Setting, then navigate to Clock.

.. image:: images/configuring_date_and_time_wearable_2.png
   :align: center

3. Tap “Time sync” and choose “Off”

.. image:: images/configuring_date_and_time_wearable_3.png
   :align: center

.. image:: images/configuring_date_and_time_wearable_4.png
   :align: center

4. Swipe right, tap “Date”, and pick the correct date. Press 
   “OK” when it is done.

.. image:: images/configuring_date_and_time_wearable_5.png
   :align: center

.. image:: images/configuring_date_and_time_wearable_6.png
   :align: center

5. Scroll down until you see “Time”. Tap it, and pick the 
   correct time. Press “OK when it is done.

.. image:: images/configuring_date_and_time_wearable_7.png
   :align: center

.. image:: images/configuring_date_and_time_wearable_8.png
   :align: center