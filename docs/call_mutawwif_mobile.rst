Call Mutawwif
=============

1. Tap ‘Profile’.

.. image:: images/call_mutawwif_mobile_1.png
   :align: center

2.  Click Mutawwif Number in the Emergency Contact.

.. image:: images/call_mutawwif_mobile_2.png
   :align: center

3.  Make call to the Mutawwif.

.. image:: images/call_mutawwif_mobile_3.png
   :align: center