View Heart Rate
===============

1. Tap ‘HR’.

.. image:: images/view_heart_rate_mobile_1.png
   :align: center

2. View pilgrim’s heart rate reading here.

.. image:: images/view_heart_rate_mobile_2.png
   :align: center