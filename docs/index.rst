Welcome to Raqib Documentation
==============================

This manual is for travel agents and mutawwif.

Raqib documentation is comprised of tutorial and guide for 
different parts of services that are offered by Raqib. The 
main documentation for the site is organized into a couple 
sections:

* :ref:`wearable-docs`
* :ref:`mobile-app-docs`
* :ref:`faq-docs`

.. _wearable-docs:

.. toctree::
   :maxdepth: 2
   :caption: Wearable Documentation

   configuring_date_and_time_wearable
   flexi-1_and_button_overview_wearable
   charging_and_power_instruction_wearable
   application_installation_wearable
   home_screen_and_main_menu_wearable
   raqib_major_function_wearable

.. _mobile-app-docs:

.. toctree::
   :maxdepth: 2
   :caption: Mobile App Documentation

   log_in_to_your_raqib_account_mobile
   reset_password_mobile
   raqib_overview_mobile
   view_latest_location_mobile
   view_heart_rate_mobile
   view_steps_mobile
   view_blood_pressure_mobile
   view_ecg_mobile
   view_menu_mobile
   view_pilgrims_information_mobile
   call_mutawwif_mobile
   view_alert_mobile
   view_about_mobile
   logging_out_mobile

.. _faq-docs:

.. toctree::
   :maxdepth: 3
   :caption: Frequently Asked Questions

   pilgrims_faq
   family_members_faq
   application_faq

.. _quick-links:

.. toctree::
   :maxdepth: 2
   :caption: Quick Links
   
   privacy_policy
   Go to Raqib Website <http://raqib.co/>
   Go to Raqib CRM <http://crm.raqib.co/>