Family Members FAQ
==================

Can I monitor my parents near real – time location in Saudi?
------------------------------------------------------------

Yes. You can monitor their location through the Raqib Mobile 
App and Raqib Dashboard.

Can I monitor my parents’ health status?
----------------------------------------

Yes. You can monitor their location through the Raqib Mobile 
App and Raqib Dashboard.

Is there any travel agent/ Tabung Haji number that I can contact via the app?
-----------------------------------------------------------------------------

Yes.

How often are the information being updated?
--------------------------------------------

Every 5 minutes.

Can I send text message to my parents through Raqib?
----------------------------------------------------

No.

Can I call my parents through Raqib?
------------------------------------

Yes. There will be International call charges by the telco.