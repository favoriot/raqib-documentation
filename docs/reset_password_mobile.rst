Reset Password
==============

1. Tap ‘Forgot/Change Password’.

.. image:: images/reset_password_mobile_1.png
   :align: center

2. Enter your Email Address as provided in your registered email
   sent by Favoriot and tap ‘Submit’.

.. image:: images/reset_password_mobile_2.png
   :align: center

3. Check your email sent from alert@favoriot.com

.. image:: images/reset_password_mobile_3.png
   :align: center

4. Enter new password.

.. image:: images/reset_password_mobile_4.png
   :align: center