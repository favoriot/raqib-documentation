View Blood Pressure
===================

1. Tap ‘BP’.

.. image:: images/view_blood_pressure_mobile_1.png
   :align: center

2. View pilgrim’s blood pressure here.

.. image:: images/view_blood_pressure_mobile_2.png
   :align: center