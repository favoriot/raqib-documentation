View Pilgrim's Information
==========================

1. Tap ‘Profile’.

.. image:: images/view_pilgrims_information_mobile_1.png
   :align: center

2. Pilgrim’s profile information displayed.

.. image:: images/view_pilgrims_information_mobile_2.png
   :align: center