Pilgrims FAQ
============

What if I misplace Raqib? Can it be found?
------------------------------------------

You can refer the last GPS location.

I think I’m lost. Can someone be able to detect my whereabouts? Will I be rescued?
----------------------------------------------------------------------------------

Yes, you need to press the SOS button. And the 
authority/mutawwif will call you for further assistance.

Can I send messages to mutawwif/my spouse/family member?
--------------------------------------------------------

No.

How can I find my mutawwif for assistance?
------------------------------------------

You can press the SOS button and an alert will be sent to the 
mutawwif.

Will Raqib replace my phone?
----------------------------

Partly, Raqib can only receive call at the moment.

Can my family member in Malaysia know my real-time location in Saudi?
---------------------------------------------------------------------

Yes. They can know through the Raqib Mobile App.

Can I take ablution with my Raqib on?
-------------------------------------

No (Not advisable).

I accidentally switched my Raqib at camp (Arafat). How can I switch it back?
----------------------------------------------------------------------------

You can call your Raqib Mobile Number to identify your Raqib.

Is there a signal coverage in Arafat?
-------------------------------------

Yes.

Does anyone monitor my health status?
-------------------------------------

Yes. Your family member and mutawwif.

How frequent do I need to charge my Raqib?
------------------------------------------

You need to charge Raqib Flexi-1 in every two days.

Can I put my Raqib on a silent mode?
------------------------------------

No. But you can immediately reject the call.