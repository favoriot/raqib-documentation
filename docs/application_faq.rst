Application FAQ
===============

What does ECG means?
--------------------

Electrocardiogram records the electrical activity of your heart 
at rest. It provides information about your heart rate and 
rhythm.

Besides health condition, what information can be seen in the app/dashboard?
----------------------------------------------------------------------------

- Location: records the wearer location up to 30 days presented 
  in heatmap form and provides actual address of the latest 
  location.
- Alerts: records geofence, sos and fall (ezy) activities.
- Wearer’s basic information and emergency contacts (mutawwif 
  and travel agent).

How can the alerts be triggered?
--------------------------------

- Geofence: when the wearer goes in or out from the designated 
  area set by the mutawwif.
- SOS: when the wearer click on the SOS button.
- Fall: when raqib(ezy) detects fall movement from the wearer.

Is there any way to contact the mutawwif or travel agent through the mobile app?
--------------------------------------------------------------------------------

Yes, the contacts is provided in the About page and can be call 
directly by clicking on the numbers.

Flexi (F-1)
-----------

What is the values in the blood pressure readings?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Systolic : The pressure in your circulatory system when your 
  heart is beating.
- Diastolic : The pressure in your circulatory system between 
  beats, when your heart is at rest.

What is the blood pressure category and its values range based on Raqib?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

+----------------------+------------+------------+
| Blood Pressure Level | Range                   |
+                      +------------+------------+
|                      | Systolic   | Diastolic  | 
+======================+============+============+
| Hypotension          |   0 - 90   |   0 - 60   |
+----------------------+------------+------------+
| Ideal                |  91 - 120  |   0 - 80   |
+                      +------------+------------+
|                      |   0 - 120  |  61 - 80   |
+----------------------+------------+------------+
| Normal               | 121 - 130  |   0 - 85   |
+                      +------------+------------+
|                      |   0 - 130  |  81 - 85   |
+----------------------+------------+------------+
| Partial High         | 131 - 140  |   0 - 90   |
+                      +------------+------------+
|                      |   0 - 140  |  86 - 90   |
+----------------------+------------+------------+
| Mild Hypertension    |  141 - 160 |   0 - 100  |
+                      +------------+------------+
|                      |   0 - 160  |  91 - 100  |
+----------------------+------------+------------+
| Prehypertension      | 161 - 180  |   0 - 110  |
+                      +------------+------------+
|                      |   0 - 180  | 101 - 110  |
+----------------------+------------+------------+
| Severe Hypertension  |   > 180    |   > 110    |
+----------------------+------------+------------+

What information can be obtained from the ECG measurement?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Heart rate: the speed at which the heart beats.
- Physiological age: a measure of how well or poorly your body 
  is functioning relative to your actual age.
- Fatigue: the level of physical exhaustion.
- ECG graph: records the rhythm of electrical activity of the 
  heart.

Ezy (E-1)
---------

What information can be obtained from the ECG measurement?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Heart rate: the speed at which the heart beats.
- Heart age: the age of your heart and blood vessels as a result 
  of your risk factors for heart attack and stroke.
- Mood: an emotional state.
- Stress: a physical, mental, or emotional factor that causes 
  bodily or mental tension.
- ECG graph: records the rhythm of electrical activity of the 
  heart.