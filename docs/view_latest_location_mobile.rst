View Latest Location
====================

1. Tap ‘Locate’.

.. image:: images/view_latest_location_mobile_1.png
   :align: center

2. View latest pilgrim’s location here.

.. image:: images/view_latest_location_mobile_2.png
   :align: center