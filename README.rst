Welcome to Raqib Documentation
==============================

|build-status| |docs|

Purpose
-------

This repository hosts manual for travel agents and mutawwif.

Currently it only has:

- Wearable documentation
- Mobile app documentation
- FAQs

Future addition:

- CRM documentation
- Dashboard documentation for end user
- Dashboard documentation for mutawwif
- Quick links for dashboard for mutawwif

It uses Sphinx_ docs written with reStructuredText_. Further 
information could be retrieved here_.

Documentation on RTD
--------------------

You will find complete documentation for Raqib at 
https://raqib.readthedocs.io/

Shared Files
------------

Here are the files that are purposely duplicated on `raqib-documentation`_ 
and `raqibuser-documentation`_:

1. configuring_date_and_time_wearable.rst
#. flexi-1_and_button_overview_wearable.rst
#. charging_and_power_instruction_wearable.rst
#. application_installation_wearable.rst
#. home_screen_and_main_menu_wearable.rst
#. raqib_major_function_wearable.rst
#. log_in_to_your_raqib_account_mobile.rst
#. reset_password_mobile.rst
#. raqib_overview_mobile.rst
#. view_latest_location_mobile.rst
#. view_heart_rate_mobile.rst
#. view_steps_mobile.rst
#. view_blood_pressure_mobile.rst
#. view_ecg_mobile.rst
#. view_menu_mobile.rst
#. view_pilgrims_information_mobile.rst
#. call_mutawwif_mobile.rst
#. view_alert_mobile.rst
#. view_about_mobile.rst
#. logging_out_mobile.rst
#. pilgrims_faq.rst
#. family_members_faq.rst
#. application_faq.rst
#. privacy_policy.rst
#. _static/css/custom_theme.css
#. _static/css/ustom_badge_only.css
#. _templates/breadcrumbs.html
#. images/application_installation_wearable_1.png
#. images/application_installation_wearable_2.png
#. images/application_installation_wearable_3.png
#. images/application_installation_wearable_4.png
#. images/call_mutawwif_mobile_1.png
#. images/call_mutawwif_mobile_2.png
#. images/call_mutawwif_mobile_3.png
#. images/docs_logo.png
#. images/favicon.png
#. images/flexi-1_and_button_overview_wearable_1.png
#. images/home_screen_and_main_menu_wearable_1.png
#. images/log_in_to_your_raqib_account_mobile_1.png
#. images/log_in_to_your_raqib_account_mobile_2.png
#. images/log_in_to_your_raqib_account_mobile_3.png
#. images/logging_out_mobile_1.png
#. images/logging_out_mobile_2.png
#. images/raqib_overview_mobile_1.png
#. images/raqib_major_function_wearable_1.png
#. images/raqib_major_function_wearable_2.png
#. images/raqib_major_function_wearable_3.png
#. images/raqib_major_function_wearable_4.png
#. images/raqib_major_function_wearable_5.png
#. images/raqib_major_function_wearable_6.png
#. images/raqib_overview_mobile_1.png
#. images/reset_password_mobile_2.png
#. images/reset_password_mobile_3.png
#. images/reset_password_mobile_4.png
#. images/view_about_mobile_1.png
#. images/view_about_mobile_2.png
#. images/view_alert_mobile_1.png
#. images/view_alert_mobile_2.png
#. images/view_alert_mobile_3.png
#. images/view_blood_pressure_mobile_1.png
#. images/view_blood_pressure_mobile_2.png
#. images/view_ecg_mobile_1.png
#. images/view_ecg_mobile_2.png
#. images/view_ecg_mobile_3.png
#. images/view_heart_rate_mobile_1.png
#. images/view_heart_rate_mobile_2.png
#. images/view_latest_location_mobile_1.png
#. images/view_latest_location_mobile_2.png
#. images/view_menu_mobile_1.png
#. images/view_menu_mobile_2.png
#. images/view_pilgrims_information_mobile_1.png
#. images/view_pilgrims_information_mobile_2.png
#. images/view_steps_mobile_1.png
#. images/view_steps_mobile_2.png

.. _Sphinx: http://sphinx.pocoo.org/
.. _reStructuredText: http://sphinx.pocoo.org/rest.html
.. _here: https://docs.readthedocs.io/en/latest/getting_started.html
.. _raqib-documentation: https://bitbucket.org/favoriot/raqib-documentation/src/master/
.. _raqibuser-documentation: https://bitbucket.org/favoriot/raqibuser-documentation/src/master/

.. |build-status| image:: https://img.shields.io/bitbucket/pipelines/atlassian/adf-builder-javascript.svg
    :alt: Build Status
    :scale: 100%
    :target: https://bitbucket.org/favoriot/raqib-documentation/src/master/

.. |docs| image:: https://img.shields.io/readthedocs/pip/stable.svg
    :alt: Documentation Status
    :scale: 100%
    :target: http://raqib.readthedocs.io/en/latest/?badge=latest